#!/bin/bash

# exit on wrong command and undefined variable
set -e
set -u
# The following one only works for bash. It catches the fail of the first
# command in a pipe. e.g. 'which asdf | true' would fail due to the missing asdf
# command.
set -o pipefail

SCRIPTPATH=$(readlink -- "$0" || echo "$0")
SCRIPTDIR=$(CDPATH= cd -- "$(dirname -- "$SCRIPTPATH")" && pwd)
PREFIX="${SCRIPTDIR}/../rtems-install/6"
export PATH="${PREFIX}/bin:${PATH}"

PORT=1234
ARCH="arm"
QEMU_NET="-net none"
QEMU_MAC=""
SCRIPTNAME=$0

# Print a help text and exit
# Arguments:
#	n.a.
printhelp () {
	echo ""
	echo "Call:   ${SCRIPTNAME} [<options>] <binary>"
	echo "Start an qemu with a given binary."
	echo ""
	echo "The following parameters are optional:"
	echo "  -h          Print this help and exit the script."
	echo "  -n <IF>     Enable network and connect to <IF>. <IF> can for"
	echo "              example be \"qtap1\"."
	echo "  -p <PORT>   Start gdb-server on given port. Use 0 for no gdb."
	echo "              Default: $PORT"
	echo ""
	exit 0
}

# generate random MAC
QEMU_MAC=`shuf -i 0-255 -n 4 -z | xargs -0 -- printf "0e:b0:%02x:%02x:%02x:%02x"`

# Read options
while getopts "hn:p:v" OPTION
do
	case $OPTION in
		h)  printhelp ;;
		n)  QEMU_NET="-net nic,model=cadence_gem,macaddr=${QEMU_MAC} -net tap,ifname=${OPTARG},script=no,downscript=no" ;;
		p)  PORT=$OPTARG ;;
		v)  VERBOSE=$(($VERBOSE + 1)) ;;
		\?) echo "Unknown option \"-$OPTARG\"."; exit -1 ;;
		:)  echo "Option \"-$OPTARG\" needs an argument."; exit -1 ;;
	esac
done
shift $(($OPTIND - 1))

# Process all parameters without dash
[[ $# -lt 1 ]] && echo "Need a binary." && exit -1

if [ $PORT -ne 0 ]
then
	QEMU_GDB="-gdb tcp::$PORT -S"
else
	QEMU_GDB="-no-reboot"
fi

#QEMU_SYSTEM="-M realview-pbx-a9 -m 256M"
QEMU_SYSTEM="-serial null -serial mon:stdio -M xilinx-zynq-a9 -m 256M"

export QEMU_AUDIO_DRV=none

echo "In QEMU: Press \"ctrl-a h\" for help."

set -x
qemu-system-arm $QEMU_GDB $QEMU_NET $QEMU_SYSTEM -nographic -kernel "$1"

# vim: set ts=4 sw=4:
