#!/bin/sh -x

user=`whoami`
interfaces=(1 2 3)

tap=qtap
bri=qbri

case $1 in
        up)
                sudo -i brctl addbr $bri
                sudo ethtool --offload  $bri  rx off  tx off
                for i in ${interfaces[@]} ; do
                        sudo -i tunctl -t $tap$i -u $user ;
                        sudo -i ip link set $tap$i up ;
                        sudo -i brctl addif $bri $tap$i ;
                done
                sudo -i ip link set $bri up
                sudo ip addr add dev $bri 172.30.0.1
                sudo ip route add 172.30.0.0/16 dev $bri
                ;;
        down)
                for i in ${interfaces[@]} ; do
                        sudo -i ip link set $tap$i down ;
                        sudo -i tunctl -d $tap$i ;
                done
                sudo -i ip link set $bri down
                sudo -i brctl delbr $bri
                ;;
        *)
                echo "Use with either 'up' or 'down'."
                ;;
esac
