#!/bin/bash

# be more verbose
set -x
# exit on wrong command and undefined variables
set -e -u

PORT=1234
SCRIPTPATH=$(readlink -- "$0" || echo "$0")
SCRIPTDIR=$(CDPATH= cd -- "$(dirname -- "$SCRIPTPATH")" && pwd)
PREFIX="${SCRIPTDIR}/../rtems-install/6"
export PATH="${PREFIX}/bin:${PATH}"

arm-rtems6-gdb \
	-x "$SCRIPTDIR/init.gdb" \
	-ex "target remote localhost:$PORT" \
	-ex "reset" \
	"$@"
