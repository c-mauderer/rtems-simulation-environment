# Note: $(PWD) doesn't work together with -C option of make.
MAKEFILE_DIR = $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

ARCH = arm
BSP = xilinx_zynq_a9_qemu
RTEMS_API ?= 6
INSTALL_PATH ?= $(MAKEFILE_DIR)/rtems-install

PREFIX = $(INSTALL_PATH)/$(RTEMS_API)
BUILD = $(MAKEFILE_DIR)/build/$(RTEMS_API)

RSB = $(MAKEFILE_DIR)/external/rtems-source-builder
SRC_LIBBSD = $(MAKEFILE_DIR)/external/rtems-libbsd
SRC_RTEMS = $(MAKEFILE_DIR)/external/rtems

export PATH := $(PREFIX)/bin:$(PATH)

.PHONY: help
#H Show this help.
help:
	@echo "See README.md for more details"
	@echo ""
	@for f in $(MAKEFILE_LIST); do echo "Commands in $$f:"; grep -v grep $$f | grep -A1 -h "#H" | sed -e '1!G;h;$$!d' -e 's/:[^\n]*\n/:\n\t/g' -e 's/#H//g' | grep -v -- --; done

.PHONY: quick-start
#H Build and install the toolchain, BSP and libraries.
quick-start: submodule-update toolchain bsp libbsd demo

.PHONY: submodule-update
#H Update Submodules. Use only if you don't want the current changes in the submodules.
submodule-update:
	git submodule update --init
	cd $(SRC_LIBBSD) && git submodule update --init rtems_waf

.PHONY: toolchain
#H Build and install toolchain
toolchain:
	rm -rf $(RSB)/rtems/build
	cd $(RSB)/rtems && ../source-builder/sb-set-builder \
	    --prefix=$(PREFIX) \
	    --log=$(RSB)/b-rsb-toolchain-$(ARCH).log \
	    $(RTEMS_API)/rtems-$(ARCH) \
	    devel/qemu-xilinx
	rm -rf $(RSB)/rtems/build

.PHONY: bsp
#H Build and install the RTEMS BSP
bsp:
	# Clean up
	rm -rf $(BUILD)/bsp
	mkdir -p $(BUILD)/bsp
	# Create config.ini
	echo "[$(ARCH)/$(BSP)]" > $(BUILD)/bsp/config.ini
	echo "RTEMS_POSIX_API = True" >> $(BUILD)/bsp/config.ini
	echo "BUILD_TESTS = True" >> $(BUILD)/bsp/config.ini
	echo "OPTIMIZATION_FLAGS = -O0" >> $(BUILD)/bsp/config.ini
	# Build BSP
	cd $(BUILD)/bsp && $(SRC_RTEMS)/waf configure \
		--out="$(BUILD)/bsp" \
		--rtems-config="$(BUILD)/bsp/config.ini" \
		--top="$(SRC_RTEMS)" \
		--prefix="$(PREFIX)"
	cd $(BUILD)/bsp && $(SRC_RTEMS)/waf install

.PHONY: libbsd
#H Build and install the RTEMS network stack (libbsd)
libbsd:
	rm -rf "$(BUILD)/libbsd"
	mkdir -p "$(BUILD)/libbsd"
	cd $(BUILD)/libbsd && $(SRC_LIBBSD)/waf configure \
	    --out="$(BUILD)/libbsd" \
	    --top="$(SRC_LIBBSD)" \
	    --prefix="$(PREFIX)" \
	    --rtems-bsps=$(ARCH)/$(BSP) \
	    --optimization=0 \
	    --enable-warnings
	cd $(BUILD)/libbsd && $(SRC_LIBBSD)/waf --targets=* install

.PHONY: demo-clean demo
#H Clean the demo application
demo-clean:
	make -C demo clean
#H Build the demo application
demo:
	make -C demo

.PHONY: tags
#H Create ctags. Used by editors like vim.
tags:
	rm -f $@
	ctags -f $@ -a \
		--extra=+f \
		--extra=+q \
		--recurse=yes \
		--languages=+Asm \
		--languages=+C \
		--languages=+C++ \
		--languages=+Python \
		--if0=yes \
		--c-kinds=+fgptx \
		--c++-kinds=+fgptx \
		--Asm-kinds=+dlmt \
		--exclude="obj-*" \
		--exclude="build" \
		--exclude="freebsd-org" \
		--exclude="rtems-source-builder" \
		.

.PHONY: run-demo
#H Run the demo application
run-demo:
	echo 'spawn sh -c "./debug/qemu.sh -p 0 demo/b-xilinx_zynq_a9_qemu/app.exe"' >> $(BUILD)/run-demo.expect
	echo 'expect "SHLL * #"' >> $(BUILD)/run-demo.expect
	echo 'send "ifconfig\n"' >> $(BUILD)/run-demo.expect
	echo 'expect "SHLL * #"' >> $(BUILD)/run-demo.expect
	echo 'send "\x04"' >> $(BUILD)/run-demo.expect
	expect $(BUILD)/run-demo.expect

.PHONY: shell
#H Start a shell with the tools in PATH.
shell:
	sh
